import keyring
import logging

class TotpStorage():
    COLLECTION_ALIAS = "OneTimeOnly"
    items = None

    def __init__(self):
        self.conn = secretstorage.dbus_init()
        self.read_items()

    def read_items(self):
        try:
            coll = secretstorage.collection.get_collection_by_alias(
                self.conn, "OneTimeOnly")
        except secretstorage.ItemNotFoundException:
            logging.warn("Did not find our Secret Store collection")
            coll = self.create_collection(self.conn)
                
        self.items = coll.get_all_items()

    def create_collection(self, label: str ="One Time Only TOTP secrets"):
        logging.warn("Creating new Secret Store collection")
        return secretstorage.collection.create_collection(label,
                                                        "OneTimeOnly")

    def create_item(self, label, secret):
        """label: str, secret: bytes"""
        try:
            coll = secretstorage.collection.get_collection_by_alias(
                self.conn, "OneTimeOnly")
        except secretstorage.ItemNotFoundException:
            logging.warn("Did not find our Secret Store collection")
            coll = self.create_collection(self.conn)
        coll.create_item(label, {}, secret)
        self.read_items()
